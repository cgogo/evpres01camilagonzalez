/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evpres01camilagonzalez;

import cl.dto.PalabraDTO;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.evpres01camilagonzalez.dao.PalabrasJpaController;
import root.evpres01camilagonzalez.entity.Palabras;

/**
 *
 * @author cGoGo
 */
@Path("buscador")
public class ApiInternaBuscador {
 
    @GET
    @Path("/{idpalabra}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response obtenerSignificado(@PathParam("idpalabra") String idPalabra) {
        
        Client client = ClientBuilder.newClient();
        
        DateTimeFormatter fecha = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime ahora = LocalDateTime.now();
        
        WebTarget diccionarioResource = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + idPalabra + "?fields=definitions");
        
        PalabraDTO palabraDTO = diccionarioResource.request(MediaType.APPLICATION_JSON).header("app_id", "1900879c").header("app_key", "fd60d8d72f017075bcabe535eb3794f6").get(PalabraDTO.class);

        String significado = (String) palabraDTO.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
        
        Palabras palabra = new Palabras();
        palabra.setPalabra(palabraDTO.getWord());
        palabra.setFecha(fecha.format(ahora));
        palabra.setSignificado(significado);
        
        return Response.ok(200).entity(palabra).build();

    }
    
}