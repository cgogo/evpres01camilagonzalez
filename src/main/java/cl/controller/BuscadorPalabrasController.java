/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import root.evpres01camilagonzalez.dao.PalabrasJpaController;
import root.evpres01camilagonzalez.entity.Palabras;

/**
 *
 * @author cGoGo
 */
@WebServlet(name = "BuscadorPalabrasController", urlPatterns = {"/BuscadorPalabrasController"})
public class BuscadorPalabrasController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BuscadorPalabrasController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BuscadorPalabrasController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);

        PalabrasJpaController dao = new PalabrasJpaController();
        String opcion = request.getParameter("accion");
        String idPalabra = request.getParameter("idPalabra");

        List<Palabras> listaPalabrasBuscadas = null;

        if (opcion.equals("buscar")) {
            Client client = ClientBuilder.newClient();
            WebTarget resource = client.target("https://evpres01camilagonzalez-app.herokuapp.com/api/buscador/" + idPalabra);
            
            Palabras resultado = (Palabras) resource.request(MediaType.APPLICATION_JSON).get(new GenericType<Palabras>(){});
            
            Palabras palabra = new Palabras();
            palabra.setFecha(resultado.getFecha());
            palabra.setPalabra(resultado.getPalabra());
            palabra.setSignificado(resultado.getSignificado());
            
            try {
                dao.create(palabra);
            } catch (Exception ex) {
                Logger.getLogger(BuscadorPalabrasController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            request.setAttribute("resultado", resultado);
            request.getRequestDispatcher("resultado.jsp").forward(request, response);
        }
        
        if (opcion.equals("verHistorial")) {
            listaPalabrasBuscadas = dao.findPalabrasEntities();
            request.setAttribute("listaPalabrasBuscadas", listaPalabrasBuscadas);
            request.getRequestDispatcher("historial.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
