<%-- 
    Document   : index
    Created on : 07-05-2021, 1:59:17
    Author     : cGoGo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Buscador de palabras</h1>
        <form name="form" action="BuscadorPalabrasController" method="POST">
            <label>Palabra a buscar</label>
            <input type="text" name="idPalabra" id="idPalabra" placeholder="Ingrese palabra"><br>
            <button type="submit" name="accion" value="buscar">Buscar palabra</button><br><br>
            <button type="submit" name="accion" value="verHistorial">Ver historial de búsqueda</button>
        </form>
        <br>
        <br>
        <p>por Camila Gonzalez</p>
        <a href="https://bitbucket.org/cgogo/evpres01camilagonzalez/src/master/">Link al repositorio</a>
    </body>
</html>
