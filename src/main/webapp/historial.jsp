<%-- 
    Document   : historial
    Created on : 08-05-2021, 22:40:50
    Author     : cGoGo
--%>

<%@page import="java.util.Iterator"%>
<%@page import="root.evpres01camilagonzalez.entity.Palabras"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Palabras> palabras = (List<Palabras>) request.getAttribute("listaPalabrasBuscadas");
    Iterator<Palabras> itPalabras = palabras.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Historial de búsqueda</title>
    </head>
    <body>
        <h1>Historial de búsquedas realizadas</h1>
        <table border="1">
            <thead>
            <th>Fecha de búsqueda</th>
            <th>Palabra</th>
            <th>Significado</th>
        </thead>
        <tbody>
            <%
                while (itPalabras.hasNext()) {
                    Palabras palabra = itPalabras.next();
            %>
            <tr>
                <td><%= palabra.getFecha()%></td>
                <td><%= palabra.getPalabra()%></td>
                <td><%= palabra.getSignificado()%></td>
            </tr>
            <% }%>
        </tbody>
    </table>
</body>
</html>
