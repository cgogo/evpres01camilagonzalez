<%-- 
    Document   : resultado
    Created on : 08-05-2021, 22:17:29
    Author     : cGoGo
--%>

<%@page import="root.evpres01camilagonzalez.entity.Palabras"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Palabras palabra = (Palabras) request.getAttribute("resultado");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado de búsqueda</title>
    </head>
    <body>
        <p>Significado de la palabra:</p>
        <h1><%= palabra.getPalabra() %></h1>
        <p><b>Significado</b></p>
        <p><%= palabra.getSignificado() %></p>
        <form name="form" action="BuscadorPalabrasController" method="POST">
            <button type="submit" name="accion" value="verHistorial">Ver historial de búsqueda</button><br><br>
        </form>
    </body>
</html>
